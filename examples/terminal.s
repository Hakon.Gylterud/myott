
 ⊢ 1 : ob ;

 A : ob  ⊢ ! A : hom A (1) ;

 A : ob , f : hom A (1) ⊢ !-uniq f : ≡ f (! A) ;
