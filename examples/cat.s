
   x : ob
 ⊢ id x : hom x x ;

   x:ob, y:ob, z:ob,
   f:hom x y, g:hom y z
 ⊢ comp f g : hom x z ;

   a:ob, b:ob,
   f:hom a b
 ⊢ left-id f : ≡ (comp (id a) f) f ;

   x:ob, y:ob,
   f:hom x y,
   i ≔ id y ,
   c ≔ comp f i
 ⊢ right-id f : ≡ c f ;

   x:ob, y:ob, z:ob, w:ob,
   f:hom x y, g:hom y z, h:hom z w
 ⊢ assoc f g h : ≡ (comp (comp f g) h)
                   (comp f (comp g h)) ;
