{-|
    Module      : Main
    Description : The main module of Myott, which calls subcommands.
    Stability   : experimental

This module contains the 'main' function which parses subcommands to execute
along with the relevant arguments from the command line.
-}

module Main where

import qualified Data.List as L
import Options.Applicative

import Driver

-- | The mode with which to run the Myott CLI.
data RunMode = CheckMode FilePath FilePath | TestMode Config FilePath FilePath

-- | Parse subcommands and relevant arguments for the Myott CLI.
parseRunMode :: ParserInfo RunMode
parseRunMode = info (subparser (checkCmd <> testCmd) <**> helper) mempty
    where
        checkCmd = command "check"
            (info (helper <*> (CheckMode <$> jFilepath <*> sFilepath))
                  (progDesc "Check specification files using a referee"))
        testCmd = command "test"
            (info (helper <*> (TestMode <$> config <*> jFilepath <*> sFilepath))
                  (progDesc "Test specific passes of Myott"))

        config = Config <$>
            option (oneOf layerOpts)
                   (long "layer" <> short 'l' <>
                    help ("Layer to test: " <> dispOpts layerOpts)) <*>
            option (oneOf layerPassOpts)
                   (long "layer-pass" <> short 'p' <>
                    help ("Pass to test in the layer: " <>
                          dispOpts layerPassOpts))

        layerOpts = [ ("judgement", JudgementLayer)
                    , ("sequent", SequentLayer)
                    , ("theory", TheoryLayer )
                    ]

        layerPassOpts = [ ("parse", ParsePass)
                        , ("check", CheckPass)
                        ]

        jFilepath = argument str $
            metavar "jfile" <> help "Source judgement file"
        sFilepath = argument str $
            metavar "sfile" <> help "Source sequent file"

        oneOf :: [(String, a)] -> ReadM a
        oneOf optPairs = eitherReader (\v -> case L.lookup v optPairs of
            Just someOpt -> Right someOpt
            _ -> Left $ "Option must be one of " <>
                        L.intercalate ", " (map (show . fst) optPairs))

        dispOpts opts = L.intercalate " | " (map fst opts)

-- | 'main' function of the Myott CLI.
main :: IO ()
main = do
    runMode <- customExecParser (prefs showHelpOnEmpty) parseRunMode
    case runMode of
        CheckMode jFilepath sFilepath -> runCheck jFilepath sFilepath
        TestMode config jFilepath sFilepath ->
            runTest config jFilepath sFilepath
