{-# LANGUAGE FlexibleInstances,MultiParamTypeClasses #-}
module Identifier where

import Util.PrettyPrint

newtype Identifier = Identifier {unIdentifier :: String }
    deriving (Eq, Show, Ord)

instance PrettyPrint Identifier where
    pprint = unIdentifier


(<+<) :: Identifier -> String -> Identifier
(Identifier i) <+< s = Identifier (i ++ s)