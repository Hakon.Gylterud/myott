{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Driver
    Description : The module exposing the subcommands of Myott.
    Stability   : experimental

This module contains the @runCheck@ and @runTest@ subcommands which can
respectively be invoked to check specifications through a referee and to test
particular passes of Myott.
-}
module Driver (
    Config (..)
  , Layer (..)
  , LayerPass (..)
  , runCheck
  , runTest
  ) where

import Text.Megaparsec
import Control.Monad.Reader
import Control.Monad.State

import Data.Functor.Identity (Identity, runIdentity)

import System.IO
import System.Exit

import qualified Data.List.NonEmpty as NonEmpty

import Control.Monad.Except
import Util.Trace

import Judgement.AST as JAST
import Judgement.Parser as Judgement
import Judgement.Referee as JRef

import Sequent.ArgumentScoping as SArgSc
import Sequent.AST as SAST
import Sequent.Monad (fromConventions)
import Sequent.Parser as Sequent
import Sequent.Referee as SRef

-- * Test-related data structures

-- | A configuration to use when running Myott.
data Config = Config { _configLayer :: Layer
                     , _configLayerPass :: LayerPass
                     }

-- | The different layers of the Myott compiler.
data Layer = JudgementLayer | SequentLayer | TheoryLayer

-- | The passes run for each layer in Myott.
data LayerPass = ParsePass | CheckPass

-- * Exposed run functions

-- | Load a judgement and a sequent file and run consistency checks on the
-- loaded specifications using a referee. If errors are found, they are logged
-- to standard output.
runCheck :: FilePath -- ^ Judgement file to load.
         -> FilePath -- ^ Sequent file to load.
         -> IO ()
runCheck jfilepath sfilepath = do
    jcontent <- openFile jfilepath ReadMode >>= hGetContents
    scontent <- openFile sfilepath ReadMode >>= hGetContents
    logErrs $ do
        -- Parse and check the specification of judgement forms
        (jspec, jconv) <- parseJudgementSpecification jfilepath jcontent
                            >>= checkJudgementSpecification jfilepath
        -- Parse and check the specification of terms
        parseSequentSpecification sfilepath scontent >>=
            checkSequentSpecification jspec jconv sfilepath

-- | Load a judgement and a sequent file and run the tests specified by the
-- configuration on the loaded. If errors are found, they are logged
-- to standard output. Note that when testing parsing of a particular layer,
-- the relevant parsing operation is always done first.
runTest :: Config
        -> FilePath -- ^ Judgement file to load.
        -> FilePath -- ^ Sequent file to load.
        -> IO ()
runTest config jfilepath sfilepath = do
    jcontent <- openFile jfilepath ReadMode >>= hGetContents
    scontent <- openFile sfilepath ReadMode >>= hGetContents
    case _configLayer config of
        JudgementLayer ->
            let jspec = parseJudgementSpecification jfilepath jcontent
            in case _configLayerPass config of
                ParsePass -> logErrs jspec
                CheckPass -> logErrs $
                    jspec >>= checkJudgementSpecification jfilepath
        SequentLayer ->
            let sspec = parseSequentSpecification sfilepath scontent
            in case _configLayerPass config of
                ParsePass -> logErrs sspec
                CheckPass -> logErrs $
                    parseJudgementSpecification jfilepath jcontent
                    >>= checkJudgementSpecification jfilepath
                    >>= \(jspec, jconv) -> sspec
                        >>= checkSequentSpecification jspec jconv sfilepath
        TheoryLayer -> error "theory layer is unimplemented"

-- * Pass-dependent functions

-- | Parse a judgement specification from a file.
parseJudgementSpecification
    :: (MonadReader [String] m, MonadError ([String], String) m)
    => FilePath -> String -> m JAST.Specification
parseJudgementSpecification jfilename jfileContent =
    tag ("Parsing the judgement specification: '" <> jfilename <> "'")
        $ either (throwWithTrace . parseErrorPretty . unBundle) return
        $ parse Judgement.specification jfilename jfileContent

-- | Check the consistency of a judgement specification using a referee.
checkJudgementSpecification jfilename jspec =
    tag ("Checking well-formedness of judgement specs: '" <> jfilename <> "'")
        $ (`runStateT` mempty)
        $ runReaderT (JAST.runSpecification jspec) JRef.protokernel

-- | Parse a sequent specification from a file.
parseSequentSpecification
    :: (MonadReader [String] m, MonadError ([String], String) m)
    => FilePath -> String -> m (SAST.Specification Identity)
parseSequentSpecification sfilename sfileContent =
    tag ("Parsing the sequent specification: '" <> sfilename <> "'")
        $ either (throwWithTrace . parseErrorPretty . unBundle) return
        $ parse Sequent.specification sfilename sfileContent

-- | Check the consistency of a sequent specification using a referee.
checkSequentSpecification jspec jconv sfilename sspec =
    tag ("Checking well-formedness of sequent specs: '" <> sfilename <> "'" <>
         " using conventions:\n" <> show jconv)
        $ runStateT (runReaderT (SAST.runSpecification
                                $ SArgSc.inSpecification sspec)
                    $ SRef.protokernel JRef.protokernel jspec) (fromConventions jconv)

-- * Miscellaneous utils

unBundle :: ParseErrorBundle s e -> ParseError s e
unBundle =  NonEmpty.head . bundleErrors

printIndented :: String -> [String] -> Int -> IO ()
printIndented istr s imax = sequence_ $ do
    (l, i) <- zip s [0..]
    let levels = min i imax
        indentation = concat (replicate levels istr)
    return $ hPutStrLn stderr (indentation ++ l)

logErrs r = case runIdentity (runExceptT r `runReaderT` []) of
    Right _ -> putStrLn "OK"
    Left (s, e) -> printIndented " " (reverse (e : s)) 10 >> exitFailure