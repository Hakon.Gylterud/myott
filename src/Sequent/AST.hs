{-|
    Module      : Sequent.AST
    Description : Abstract syntax trees for operations.
    Stability   : experimental

This module defines abstract syntax trees for operation
specifications and functions to run these on a
'Sequent.Referee'.

The abstract syntax is produced by the parser, but is not
the final product. The abstract syntax is fed to a referee,
which produces a complete combintatorial representation of the
specification.

The heavy lifting in this section is done by 'resolveArguments' and
'constructArguments', which transform the tree structure of the
abstract syntax to a sequential, flattened representation which
is fed through a Referee.

On a syntactic level, what happens is that something like:

@
      x:ob, y:ob,
      f:hom x y,
     ⊢ right-id : ≡ x y (comp x y y f (id y)) f ;
@

is elaborated to:

@
        x:ob, y:ob,
        f:hom x y,
        i ≔ id y ,
        c ≔ comp x y y f i
     ⊢ right-id : ≡ x y c f ;
@

The recursive term @≡ x y (comp x y y f (id y)) f@ is flattened
by introducing intermediate definitions (@i@ and @c@).
-}

module Sequent.AST where

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import qualified Data.Set as Set

import Control.Monad.Reader
import Control.Monad.State

import Data.Maybe (fromMaybe)

import qualified Judgement
import qualified Sequent
import Sequent.Monad
import Util.Trace
import Util.PrettyPrint
import Util.Monad
import qualified Argument
import Argument.Conventions (nameArguments
                            ,inferArguments
                            ,insertConvention)
import Argument.Inference

import Identifier

type Arguments a = Argument.Arguments Identifier a


{- | An abstract, syntactic term consists of a head
     (the identifier of the outermost operation in
     the term construction) and a set of arguments,
     which are either names of elements in the environment
     or themselves terms.

     The data type argument is used for different versions
     of the AST during different phases of parsing a
     specification and feeding it to a referee. -}
data Term f = Term {
     termHead :: Identifier, {- ^ The identifier of the outermost operation in
                                   the term construction -}
     termArgs :: Arguments (f (Term f))
                              {- ^ The arguments to the head of the term.-}
     }

{- | An abstract, syntactic judgement in an environment. -}
data Judgement f
     = Judgement Identifier
                 (Arguments (f (Term f)))

{- | An abstract, syntactic environment consisting of
     assumptions and terms. -}
newtype Environment f
     = Environment [(Identifier, Either (Judgement f) (Term f))]

{- | An abstract, syntactic sequent defining an operation.-}
data Sequent f = Sequent (Environment f)
                         Identifier
                         [Identifier]
                         (Judgement f)

{- | An abstract, syntactic specification. -}
newtype Specification f = Specification [Sequent f]

-- | Monad stack for running environments through a referee.
type MakeEnvironment s e j t a
     = StateT e (ReaderT (Referee s e j t) ConventionMonad) a

-- Run the Conventions monad on specific subsets of the state.
onTerms = onSubState getTerms setTerms
onConventions = onSubState getConventions setConventions

modifyM f
     = do s <- get
          s' <- lift (f s)
          put s'

suffix = map show [0..]

freshest prefix l
     = head
     $ filter (not . (`elem` l))
     $ (prefix <+<) <$> suffix


-- | Produce a fresh variable in an environment based on a prefix.
fresh :: Identifier -- ^ A prefix for the fresh variable.
      -> e          -- ^ The current environment.
      -> MakeSys s e j t Identifier
fresh prefix e
     = do env <- asks Sequent.environment <*> pure e
          return . freshest prefix $
               Judgement.varToIdentifier <$> Map.keys (Sequent.terms env)


-- | Check if a given term construction already exists in an environment.
checkConstructed :: Identifier -- ^ The identifier of the operation.
                 -> Map Identifier Identifier -- ^ The arguments to the operation.
                 -> e -- ^ The current environment
                 -> MakeSys s e j t (Maybe Identifier) -- ^ An identifier for the term if already defined.
checkConstructed h as e
     = do env <- asks Sequent.environment <*> pure e
          -- TODO: Sort this mess of converting from Constant and Variable to
          --       identifier.
          let m = Map.mapKeysMonotonic (\t-> (Sequent.constantToIdentifier (Sequent.head t) , Map.mapKeysMonotonic Judgement.varToIdentifier $ Map.map Judgement.varToIdentifier $ Sequent.arguments t)) (Sequent.representation env) :: Map (Identifier, Map Identifier Identifier) Judgement.Variable
          return $ Judgement.varToIdentifier <$> Map.lookup (h,as) m

{- | Construct a term from its head and arguments through a Referee.
     The juice of 'construct' is that it only runs through the Referee
     if the term is not already constructed. -}
construct :: Identifier  -- ^ The identifier of the operation.
          -> Map Identifier Identifier  -- ^ The arguments to the operation
          -> MakeEnvironment s e j t Identifier
          -- ^ The identifier of the constructed term.
construct h is
     = do e <- get
          v <- lift $ checkConstructed h is e
          maybe (do i <- lift $ fresh h e
                    modifyM (define i <^ apply h is)
                    return i)
                return v


-- | Recursively construct all the arguments of an operation.
constructArguments :: Identifier -- ^ The identifier of the operation.
                   -> Arguments (Either Identifier (Term (Either Identifier)))  -- ^ The recursive arguments of the operation.
                   -> MakeEnvironment s e j t (Map Identifier Identifier)
                   {- ^ The flattened arguments of the operation, now constructed
                        in the current context.-}
constructArguments h as
     = do ts <- lift $ lift $ onConventions (nameArguments h as)
          is <- forM ts $ either return $
               \t -> do is' <- constructArguments (termHead t) (termArgs t)
                        construct (termHead t) is'
          e <- get
          env <-  lift $ asks $ ($ e) . Sequent.environment
          let context = Judgement.dependencies (Sequent.context env)
          is' <- lift $ lift $ onConventions $ inferArguments context h is

          let unwrapTerms (ConventionsAndTerms _ t) = t
          terms <- lift $ gets (fromMaybe Map.empty . Map.lookup h . unwrapTerms)
          let mapTerms =
                forM (mapKeysFromVarToId terms)
                (\term -> constructTerm terms term is')

          Map.union is' <$> mapTerms

-- TODO This could be rewritten to avoid unnecessary (attempted) reconstructions
-- of the same terms over and over again.
-- E.g. when constructing the terms
-- x := op1 y
-- y := op2
-- we might try to construct x first, which will recursively construct y.
-- Then after x is constructed, we will (from constructArguments) try to
-- construct y again, because we didn't do any memoization.
constructTerm :: Sequent.Terms
--                   ^-- All the terms defined in the current environment.
              -> Sequent.Term
--                   ^-- The current term to be constructed
--                      (Note that the Term arguments are mappings of variables
--                       into the env of the sequent this term belongs to, yet
--                       we want to map them into the env of the current sequent)
              -> Map Identifier Identifier
--                   ^-- Map of inferred arguments for the sequent being "called".
              -> MakeEnvironment s e j t Identifier
constructTerm terms term translation = do
  constructedArguments <- traverse (\varName -> do
        let varName' = Judgement.varToIdentifier varName
        case Map.lookup varName' translation of
          -- The argument was a variable, so it has already been mapped into
          -- the current context, where it has name @v@.
          Just v -> pure v
          -- The argument was another term, so we need to construct that term.
          Nothing -> case Map.lookup varName terms of
                (Just t) -> constructTerm terms t translation
                Nothing -> lift $ lift $ throwWithTrace $ "BUG #14: Could not find "
                                   ++ pprint varName
                                   ++ " in " ++ pprint terms
                                   ++ "The operation " ++ pprint (Sequent.head term)
                                   ++ " should probably take more arguments."

        ) (mapKeysFromVarToId $ Sequent.arguments term)
  let termName = Sequent.constantToIdentifier $ Sequent.head term
  construct termName constructedArguments

mapKeysFromVarToId = Map.mapKeysMonotonic Judgement.varToIdentifier

runTerm :: Term (Either Identifier) -- ^ The term to run through a referee.
        -> e    -- ^ The current environment
        -> MakeSys s e j t t -- ^ The refereed term.
runTerm (Term h as) e
     = do (is, e') <- runStateT (constructArguments h as) e
          apply h is e'

runJudgement :: Judgement (Either Identifier)  -- ^ The judgement to construct
             -> e          -- ^ The current enviroment
             -> MakeSys s e j t j
                           -- ^ The refereed judgement
runJudgement = evalStateT . runJudgement'

runJudgement' :: Judgement (Either Identifier)  -- ^ The judgement to construct
             -> MakeEnvironment s e j t j
                           -- ^ The refereed judgement
runJudgement' (Judgement h as)
     = do is <- constructArguments h as
          e' <- get
          lift $ applyForm h is e'

runEnvironment :: Environment (Either Identifier)  -- ^ The enviroment to construct
               -> s            -- ^ The current specification
               -> MakeSys s e j t e
                               -- ^ The refereed environment
runEnvironment (Environment ts)
     = environment
          ^> chain (map (\(i , k)
               -> either (\j -> assume i <^ runJudgement j)
                         (\t -> define i <^ runTerm t) k) ts)

runSequent :: Sequent (Either Identifier)
           -> s
           -> MakeSys s e j t s
runSequent (Sequent env name arguments judgement) s
     = mapReaderT (tag ("In the definition of '" ++ pprint name ++ "':"))
     $ do e <- runEnvironment env s
          env' <- asks $ ($ e) . Sequent.environment
          let context = Judgement.dependencies (Sequent.context env')
              res = createResolution context (Set.fromList arguments)
          (j, e') <- runStateT (runJudgement' judgement) e
          s' <- declare name j

          lift $ onConventions $ modify (insertConvention name arguments res)

          terms <- asks $ (Sequent.terms . ($ e')) . Sequent.environment
          lift $ onTerms $ modify (Map.insert name terms)

          return s'

runSpecification :: Specification (Either Identifier)
                      -- ^ The specification to run through the referee
                 -> MakeSys s e j t s
                      -- ^ The refereed specification
runSpecification (Specification ss)
     = system $ chain (map runSequent ss)
