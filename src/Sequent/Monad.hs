module Sequent.Monad where

import Control.Monad.Reader
import Control.Monad.State
import Prelude hiding (init,print)

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

import Util.Trace

import qualified Sequent
import qualified Judgement

import Identifier

import qualified Argument.Conventions

type Conventions
    = Argument.Conventions.Conventions
            Identifier
            Identifier
            Identifier

data ConventionsAndTerms = ConventionsAndTerms
    Conventions
    (Map Identifier Sequent.Terms)
--    ^ maps 'operation name' -> 'terms of that operation'.
    deriving Show

fromConventions :: Conventions -> ConventionsAndTerms
fromConventions conv = ConventionsAndTerms conv Map.empty

getTerms :: ConventionsAndTerms
         -> Map Identifier Sequent.Terms
getTerms (ConventionsAndTerms _ terms) = terms

setTerms :: Map Identifier Sequent.Terms
         -> ConventionsAndTerms
         -> ConventionsAndTerms
setTerms terms (ConventionsAndTerms conv _) =
    ConventionsAndTerms conv terms

getConventions :: ConventionsAndTerms -> Conventions
getConventions (ConventionsAndTerms conv _) = conv

setConventions :: Conventions -> ConventionsAndTerms -> ConventionsAndTerms
setConventions conv (ConventionsAndTerms _ terms) =
    ConventionsAndTerms conv terms

type ConventionMonad
    = StateT ConventionsAndTerms (TraceError String String)

type Referee specification environment judgement term
    = Sequent.Referee   ConventionMonad
                        specification
                        environment
                        judgement
                        term

type MakeSys specification environment judgement term a
    = ReaderT (Referee specification environment judgement term)
              ConventionMonad
              a

dependency :: Identifier -> Identifier -> e -> MakeSys s e j t Identifier
dependency d v e = ReaderT (\k -> Judgement.varToIdentifier <$> Judgement.dependency (Sequent.context (Sequent.environment k e)) d v)


init :: MakeSys s e j t s
environment :: s -> MakeSys s e j t e
init           = ReaderT (return . Sequent.init)
environment s      = ReaderT (\k -> return $ Sequent.empty k s)
declare n j = ReaderT (\k -> Sequent.declare k n j)
assume n j     = ReaderT (\k -> Sequent.assume k n j)
define n t     = ReaderT (\k -> Sequent.define k n t)
applyForm h as e   = ReaderT (\k -> Sequent.applyForm k h as e)
apply h as c   = ReaderT (\k -> Sequent.apply k h as c)

noArgs :: (Ord a) => Map a b
noArgs = Map.empty

args :: (Ord a) => [(a,b)] -> Map a b
args = Map.fromList

system d = init >>= d
