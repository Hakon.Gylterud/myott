{-|
    Module      : Sequent.ArgumentScoping
    Description : Functions for doing argument scoping in sequents.
    Stability   : experimental

This module contains functions for deciding if the arguments in the judgements
and terms in sequents are variables.

The parsers in "Sequent.Parser" construct an AST in which all arguments are
considered terms (possibly with an empty list of arguments). The functions in
this module are then applied to that AST to produce a new AST in which some
arguments are variables.
-}

module Sequent.ArgumentScoping (inSpecification) where

import Data.Bifunctor ( Bifunctor(bimap) )
import Data.Functor.Identity ( Identity(..) )
import Data.Set (Set)
import qualified Data.Set as Set

import Argument
import Identifier
import Sequent.AST

-- | Given a set of variables, decide if an argument is a variable or a term.
scopeArgument :: Set Identifier                    -- ^ Set of variables.
              -> Identity (Term Identity)          -- ^ Unscoped argument.
              -> Either Identifier 
                        (Term (Either Identifier)) -- ^ Scoped argument.
scopeArgument variables (Identity (Term hd arguments))
    | null arguments = if hd `Set.member` variables
                          then Left hd
                          else Right (Term hd (ArgList []))
    | otherwise = Right (Term hd (scopeArgument variables <$> arguments))

-- | Scope all arguments in all judgements and terms in a sequent.
inSequent :: Sequent Identity            -- ^ Sequent with unscoped arguments.
          -> Sequent (Either Identifier) -- ^ Sequent with scoped arguments.
inSequent (Sequent (Environment envList) hd args judgement)
    = let variables = Set.fromList $ map fst envList
          inJudgement (Judgement jgmtHead jgmtArgs)
              = Judgement jgmtHead (scopeArgument variables <$> jgmtArgs)
          inTerm (Term tmHead tmArgs)
              = Term tmHead (scopeArgument variables <$> tmArgs)

      in  Sequent (Environment [(ident, bimap inJudgement inTerm jgmtOrTerm)
                               | (ident, jgmtOrTerm) <- envList])
                  hd
                  args
                  (inJudgement judgement)

-- | Scope all arguments in all sequents in a sequent specification.
inSpecification :: Specification Identity            {- ^ Specification with
                                                          unscoped arguments. -}
                -> Specification (Either Identifier) {- ^ Specification with
                                                          scoped arguments. -}
inSpecification (Specification sequentList)
    = Specification (map inSequent sequentList)
