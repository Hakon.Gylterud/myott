{-|
    Module      : Sequent.Parser
    Description : Parser for sequent specifications.
    Stability   : experimental

This module defines parsers for sequent specifications. In the resulting AST,
all arguments are cosidered terms (possibly with an empty list of arguments).
-}

module Sequent.Parser where

import Data.Functor.Identity (Identity(Identity))
import Prelude hiding (init,print)

import Text.Megaparsec
import Text.Megaparsec.Char

import Argument
import Sequent.AST
import Util.Parser
import Util.Monad

{- | Parse a term. For example:

@comp f (id x)@ -}
term :: Parser (Term Identity)
term = Term <$> identifier
            <*> arguments (Identity <$> term)
                          (Identity <$> ((Term <$> identifier <*> return (ArgList []))
                                        <|> betweenParens term))

{- | Parse a judgement. For example:

@Hom {dom = x, codom = y}@ -}
judgement :: Parser (Judgement Identity)
judgement = Judgement <$> identifier
                      <*> arguments (Identity <$> term)
                                    (Identity <$> ((Term <$> identifier <*> return (ArgList []))
                                                  <|> betweenParens term))

{- | Parse an environment. For example:

@x : Ob, y : Ob, f : Hom x y@ -}
environment :: Parser (Environment Identity)
environment
    = Environment
        <$> (do
            x <- identifier
            space
            s <- eitherP (char ':') (char '≔')
            space
            p <- either (const (Left <$> judgement)) (const (Right <$> term)) s
            space
            return (x,p)) `sepBy` (char ',' >> space)

{- | Parse a sequent. For example:

@
  x : Ob, y : Ob, f : Hom x y, i ≔ id x
⊢ right-id f : ≡ {lhs = comp f i, rhs = f} ;
@ -}
sequent :: Parser (Sequent Identity)
sequent
    = Sequent
        <$> environment
        <*> (char '⊢' >> space >> identifier)
        <*> (space >> identifier `sepBy'` space1)
        <*> (space >> char ':' >> space >> judgement << space << char ';')

-- | Parse a sequent specification.
specification :: Parser (Specification Identity)
specification
    = Specification <$> many (space >> sequent << space)
