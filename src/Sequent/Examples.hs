module Sequent.Examples where
import Prelude hiding ((<>))
import Identifier
import Util.Monad
import qualified Data.Map.Strict as Map

import qualified Sequent.Monad as M
import Sequent.Monad hiding (apply, assume, define, declare, applyForm, dependency)


dependency d v e = unIdentifier <$> M.dependency (Identifier d) (Identifier v) e
apply n m
   = M.apply (Identifier n)
             (Map.map Identifier $ Map.mapKeysMonotonic Identifier m)
assume = M.assume . Identifier
define = M.define . Identifier
declare = M.declare . Identifier
applyForm n m
   = M.applyForm (Identifier n)
                 (Map.map Identifier $ Map.mapKeysMonotonic Identifier m)

{- Example -}

-- WARNING:
-- These use the MLTT spec, but in general trying
-- to specify type theory using Sequent.Monad is
-- futile, since there cannot be any quantifiers.

declareNat :: s -> MakeSys s e j t s
declareNat = declare "ℕ" <^ (environment ^>
                             applyForm "type" noArgs)

formNat :: e -> MakeSys s e j t e
formNat = define "ℕ" <^ apply "ℕ" noArgs


declareZero :: s -> MakeSys s e j t s
declareZero = declare "0" <^ applyForm "element" (args [("A","ℕ")])
                          <^ formNat
                          <^ environment

formZero :: e -> MakeSys s e j t e
formZero = define "0" <^ apply "0" (args [("ℕ","ℕ")])

declareSucc :: s -> MakeSys s e j t s
declareSucc = declare "succ" <^ applyForm "element" (args [("A","ℕ")])
                             <^ (assume "x" <^ applyForm "element" (args [("A","ℕ")]))
                             <^ formNat
                             <^ environment

formSucc :: String -> String -> e -> MakeSys s e j t e
formSucc n sn = define sn <^ apply "succ" (args [("ℕ","ℕ"),("x",n)])


formTwo = formNat ^>
          formZero ^>
          formSucc "0" "1" ^>
          formSucc "1" "2"

{- Category -}


ob = applyForm "Ob" (args [])

hom a b = applyForm "Hom" (args [("dom",a),("codom",b)])

eq f g e = do
   a <- dependency "dom" f e
   b <- dependency "codom" f e
   applyForm "≡" (args [("lhs", f),("rhs", g),("dom",a),("codom",b)]) e

declareComposition :: s -> MakeSys s e j t s
declareComposition =
   declare "compose" <^ hom "a" "c"
                     <^ (environment ^>
                         assume "a" <^ ob ^>
                         assume "b" <^ ob ^>
                         assume "c" <^ ob ^>
                         assume "f" <^ hom "a" "b" ^>
                         assume "g" <^ hom "b" "c")

g <> f = \e -> do
   a <- dependency "dom" f e
   b <- dependency "codom" f e
   c <- dependency "codom" g e
   apply "compose" (args [("f",f),("g",g),("a",a),("b",b),("c",c)]) e

declareIdentity =
   declare "id" <^ hom "a" "a"
                <^ (environment ^>
                    assume "a" <^ ob)
id' x =
  apply "id" (args [("a",x)])


declareAssociativity =
   declare "assoc" <^ eq "composition₁" "composition₂"
                   <^ (environment ^>
                       assume "a" <^ ob ^>
                       assume "b" <^ ob ^>
                       assume "c" <^ ob ^>
                       assume "d" <^ ob ^>
                       assume "f" <^ hom "a" "b" ^>
                       assume "g" <^ hom "b" "c" ^>
                       assume "h" <^ hom "c" "d" ^>
                       define "composition₁-left"  <^ ("h" <> "g") ^>
                       define "composition₂-right" <^ ("g" <> "f") ^>
                       define "composition₁" <^ ("composition₁-left" <> "f") ^>
                       define "composition₂" <^ ("h" <> "composition₂-right"))
