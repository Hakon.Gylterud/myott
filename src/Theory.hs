{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Sequent
    Description : Data types for theories and interface for referee.
    Stability   : experimental

This module defines a combinatorial representation of
a theory specification consisting of sequents, as
well as an interface, 'Theory.Referee',
to safely construct such specifications.
-}
module Theory where

import Prelude hiding (head)

import Judgement (Judgement,Variable)
import Sequent (Environment, Constant, Term, System)
import qualified Sequent as S
import qualified Data.Map.Strict as Map
import Data.Map (Map)

import Util.Trace
import Util.PrettyPrint
import Identifier

{- 'Rule' encapsulates the Identifier for a rule. -}
newtype Rule = Rule Identifier
    deriving (Eq,Ord,Show)

{- | 'Application' represents applications of a rule in a 'Frame'. -}
data Application =
    Application {
        head :: Rule,
        parameters :: Map Constant (Constant, (Map Variable Variable, Map Variable Variable)),
        arguments  :: Map Variable Variable
    }
    deriving (Eq,Show,Ord)

{- | 'Frame' represents the elements “above the line” in a rule.
     The elements of the frame are either assumed operations or
     applications of rules.-}
data Frame =
    Frame {
        -- | The underlying operation specification.
        system :: System,
        {- | The mapping between constants which are constructed
            by applications of other rules, and their definition. -}
        applications :: Map Constant Application,
        -- Reverse mapping of applications of rules.
        applicationsInverse :: Map Application Constant}
    deriving (Eq,Show,Ord)

{- | 'Theory' represents a collection of rules.-}
newtype Theory =
    Theory (Map Rule ((Frame,Environment),Judgement))
    deriving (Eq,Show,Ord)

definition h (Theory m)
    = let r = Rule h in
      case Map.lookup r m of
        Nothing -> throwWithTrace $ "Undefined rule: " ++ pprint h
        (Just x) -> return x

{- | 'Theory.Referee' is an interface for safely constructing
     speficiations of theories. -}
data Referee m theory frame environment judgement application term =
    Referee {
        {- | Navigate up in the specification along
            the dependencies: Applications and terms have an underlying judgement,
            judgements lie in an environment, environments are
            relative to a frame and frames are relative to a theory.-}
        deps            :: ((((application -> judgement, term -> judgement), judgement -> environment), environment -> frame), frame -> theory),
        -- | Realise the constructed 'Theory'.
        theory          :: theory -> Theory,
        -- | Realise the constructed 'Frame'.
        frame           :: frame -> Frame,
        -- | Realise the constructed 'Environment'.
        environment     :: environment -> Environment,
        -- | Realise the constructed 'Judgement'.
        judgement       :: judgement -> Judgement,
        -- | Realise the constructed 'Term'.
        term            :: term -> Term,
        -- | Realise the constructed 'Application'.
        application     :: application -> Application,
        -- | Initialise a theory.
        initTheory      :: theory,
        -- | Initialise a frame.
        initFrame       :: theory -> frame,
        -- | Start an empty frame.
        emptyEnv        :: frame -> environment,
        -- | Define a operation based on an application of a rule.
        defineInFrame   :: Identifier -> application -> m frame,
        -- | Declare an assumed operation in a frame.
        declareConstant :: Identifier -> judgement -> m frame,
        {- | Assume a new variable. -}
        assumeVariable  :: Identifier -> judgement -> m environment,
        {- | Define a term in an environment. -}
        defineInEnv     :: Identifier -> term -> m environment,
        {- | Define a new rule. -}
        declare         :: Identifier -> judgement -> m theory,
        {- | Form a new judement in an environment by applying a judgement form
            to some arguments. -}
        applyForm       :: Identifier -> Map Identifier Identifier -> environment -> m judgement,
        {- | Form a term in an environment by applying an operation
            to some arguments. -}
        applyConstant   :: Identifier -> Map Identifier Identifier -> environment -> m term,
        {- | Form an operation in a frame by applying a rule
            to some arguments. -}
        applyRule       :: Identifier -> Map Identifier (Identifier, (Map Identifier Identifier,Map Identifier Identifier))
                        -> Map Identifier Identifier -> environment -> m application
    }


extFrameDef h e j a f@(Frame s m r)
    = do
        (s',c) <- S.extSys h e j s
        case (Map.lookup c m, Map.lookup a r) of
            (Nothing, Nothing) ->
                return (Frame s' (Map.insert c a m) (Map.insert a c r),c)
            (Just _, _) -> throwWithTrace
                $ "The constant '" ++ pprint h ++ "' is already defined in "
                  ++ "frame\n'" ++ pprint f ++"'."
            (_, Just c') -> throwWithTrace
                $ "The constant '" ++ pprint c' ++ "' already represents the "
                  ++ "application '" ++ pprint a ++ "'."

extTheory h f e j t@(Theory m) =
    let r = Rule h in
    case Map.lookup r m of
        Nothing -> return (Theory (Map.insert (Rule h) ((f,e),j) m),r)
        (Just _) -> throwWithTrace
            $ "A rule with the name '" ++ pprint h ++ "' is already defined "
              ++ "in the theory:\n'" ++ pprint t ++"'"

instance PrettyPrint Rule where
    pprint (Rule r) = pprint r

instance PrettyPrint Frame where
    pprint (Frame (S.System dfs) df _)
        = unlines $ map (\(h,(c,jk)) -> unwords $ [pprint c, "⊢", pprint h]
                                                  ++ def h ++ [" : ",pprint jk])
                        (Map.toList dfs)
            where
                def h = case Map.lookup h df of
                    Nothing -> []
                    Just app -> [" := " ++ pprint app]

instance PrettyPrint Theory where
    pprint (Theory m) = unlines
        $ map (\(r,((f,e),j)) ->
                let conclusion = pprint e ++ " ⊢ " ++ pprint r ++ " : "
                                 ++ pprint j
                in pprint f ++ take (maximum $ (length conclusion:)
                                             $ map length $ lines $ pprint f)
                                    (cycle "─")
                   ++ " Rule: " ++ pprint r ++ "\n" ++ conclusion ++ "\n" )
              (Map.toList m)

instance PrettyPrint Application where
    pprint (Application h ps as)
        = pprint h ++ " "
          ++ unwords (map (\(c,(c',(vs,_))) ->
                                "(" ++  pprint c ++ " = " ++ pprint c'
                                ++ assignmentList (Map.toList vs))
                          (Map.toList ps))
          ++ assignmentList (Map.toList as)