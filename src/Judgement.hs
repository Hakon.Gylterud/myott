{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Judgement
    Description : Data types for judgement forms and interface for referee.
    Stability   : experimental

This module defines a combinatorial representation of
a judgement form specification and an interface, 'Judgement.Referee',
to safely construct such specifications.
-}
module Judgement
  (JudgementForm,
   Variable(Variable), JudgementKind(Sort,Proposition),
   Specification(Specification), Context(Context),
   structure,reverseStructre, Referee(Referee),
   varToIdentifier, judgeFormToIdentifier, head, arguments,
   variables, kind, form, match, dependency, dependencies,
   definition, extSpec, emptyCtx, extCtx,
   Judgement(Judgement), deps, specification,
   context, judgement, init, empty, declare,
   assume, apply) where
import Prelude hiding (init,head)
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import qualified Data.Set as Set
import Data.Set (Set)
import Data.Maybe
import Control.Monad.Reader
import Control.Monad.Except

import Util.Trace
import Util.PrettyPrint
import Argument (DependencyRelation)

import Identifier


{- | A variable is an Identifier for an element in
     the context of a judgement -}
newtype Variable = Variable Identifier
    deriving (Eq,Ord,Show)

{- | The JudgementForm datatype encapsulates the
     identifier for a single judgement form in
     the specification. -}
newtype JudgementForm = JudgementForm Identifier
    deriving (Eq,Ord,Show)

{- | There are two kinds of judgements, proof relevant sorts
     and proof irrelevant propositions. 'JudgementKind' defines
     these.
-}

data JudgementKind = Sort | Proposition
    deriving (Eq,Ord,Show)


{- | The 'Judgement' data type encapsulates a specific
     Judgement relative to a 'Context'.
-}

data Judgement =
    Judgement {
        head :: JudgementForm,
        arguments :: Map Variable Variable,
        kind :: JudgementKind -- For convenience.
    }
    deriving (Eq,Ord,Show)

{- | A 'Context' records what judgements variables represent.
     We keep a reverse map as well, for convenience to check
     if a proposition has already been proven. -}

data Context =
    Context {
        structure :: Map Variable Judgement,
        reverseStructre ::  Map Judgement (Set Variable)
    }
    deriving (Eq,Ord,Show)


{- | A 'Specification' records what arguments a judgement form takes
and what kind of judgement it is. -}

newtype Specification =
    Specification (Map JudgementForm (Context,JudgementKind))
    deriving (Eq,Ord,Show)

{- | A judgement referee is a set of abtract types for producing
     verified judgement declarations. The 'Referee' data structure
     provides an abstraction barrier preventing proofs from taking
     advantage of the internal structure of context etc.
     (aka. kernel a là Bauer)

     This abstraction is provided throught the parameters s c and j,
     which abstractly represents 'Specification', 'Context', and
     'Judgement', but prevents construction of such elements from
     the outside.

     The interface is parameterized with a monad, m, which allows
     the referee to return errors (or other side effects).

 -}

data Referee m spec context judement =
    Referee {
        {- | Navigate up in the specification along
            the dependencies: Judgements are in a
            context, contexts are in a specification. -}
        deps          :: (judement -> context, context -> spec),
        {- | Realise the concrete 'Specification'. -}
        specification :: spec -> Specification,
        {- | Realise the concrete 'Context'. -}
        context       :: context -> Context,
        {- | Realise the concrete 'Judgement'. -}
        judgement     :: judement -> Judgement,
        {- | Initialise a specification. -}
        init          :: spec,
        {- | Start an empty context. -}
        empty         :: spec -> context,
        {- | Declare a new judgement form. -}
        declare       :: Identifier -> JudgementKind -> context -> m spec,
        {- | Assume a new variable. -}
        assume        :: Identifier -> judement -> m context,
        {- | Form a new judement in a context by applying a judgement form
            to some arguments. -}
        apply         :: Identifier -> Map Identifier Identifier -> context -> m judement
    }


{- Utility functions -}

varToIdentifier :: Variable -> Identifier
varToIdentifier (Variable s) = s

judgeFormToIdentifier :: JudgementForm -> Identifier
judgeFormToIdentifier (JudgementForm n) = n

{- Context construction -}


emptyCtx :: Context
emptyCtx = Context Map.empty Map.empty

{- Context construction -}

extCtx :: (MonadReader [String] m, MonadError ([String],String) m)
       => Identifier
       -> Judgement
       -> Context
       -> m (Context,Variable)
extCtx ident jgmt c@(Context ctx revCtx) = do
    let var = Variable ident
    when (kind jgmt == Proposition) $
        case Set.lookupMin (match c jgmt) of
            Nothing -> return ()
            Just v' -> throwWithTrace
                $ "Cannot assume proposition '" ++ pprint ident ++ " "
                  ++ pprint jgmt ++ "'.\n" ++ "  The proof object '"
                  ++ pprint v' ++ " " ++ pprint jgmt ++ " is already in the "
                  ++ "context '" ++ pprint c ++ "'."
    when (isJust $ Map.lookup var ctx) $
        throwWithTrace $ "The variable '" ++ pprint ident ++ "' is already "
                         ++ "defined in the context '" ++ pprint c ++"'."
    let newCtx    = Map.insert var jgmt ctx
        newRevCtx = Map.alter
            (Just . maybe (Set.singleton var) (Set.insert var)) jgmt revCtx
    return (Context newCtx newRevCtx, var)

{- Context query -}

-- | Get the variables in the context.
variables :: Context -- ^ The context to query.
          -> Set Variable -- Set of variables in the context.
variables (Context c _) = Map.keysSet c

-- | Lookup a variable and get the belonging 'Judgement' in a context.
form :: (MonadReader [String] m, MonadError ([String],String) m)
     => Identifier -- ^ The name of the variable.
     -> Context   -- ^ The context to query.
     -> m (Variable, Judgement) -- If the variable is in context, return it
                                -- along with its form and arguments.
form s (Context c r)
    = let v = Variable s in
      case Map.lookup v c of
          Nothing -> throwWithTrace
            $ "No variable '" ++ pprint s ++ "' in context '"
              ++ pprint (Context c r) ++ "'."
          (Just x) -> return (v,x)

-- | Look up a dependency of an element of a context.
dependency :: (MonadReader [String] m, MonadError ([String],String) m)
           => Context   -- ^ The context to query
           -> Identifier -- ^ The name of the dependency
           -> Identifier -- ^ The name of the variable
           -> m Variable -- The variable pointed to by the dependency.
dependency c d v
    = do
        (_,x) <- form v c
        case Map.lookup (Variable d) (arguments x) of
            Just y -> return y
            Nothing -> throwWithTrace
                $ "There is no dependency '" ++ pprint d ++ "' for '"
                  ++ pprint v ++"' which is of form '" ++ pprint x ++ "'."

dependencies :: Judgement.Context
    -> DependencyRelation Identifier Identifier
dependencies
    = Map.map (  Map.map Judgement.varToIdentifier
               . Map.mapKeysMonotonic Judgement.varToIdentifier
               . Judgement.arguments )
    . Map.mapKeysMonotonic Judgement.varToIdentifier
    . Judgement.structure

-- | Look up all variables witnessing a judgement in a context.
match :: Context -- ^ The context to query
      -> Judgement -- ^ The judgement to look up
      -> Set Variable -- All variables witnessing the given judgement.
match (Context _ r) j = fromMaybe Set.empty (Map.lookup j r)

{- Specification construction -}

extSpec :: (MonadReader [String] m, MonadError ([String],String) m)
        => Identifier -> Context -> JudgementKind -> Specification
        -> m Specification
extSpec n c b (Specification s)
    = case Map.lookup (JudgementForm n) s of
        Nothing -> return $ Specification (Map.insert (JudgementForm n) (c,b) s)
        Just _ -> throwWithTrace
            $ "The judgement-form '" ++ pprint n ++ "' is already defined."

{- Specification query -}

definition :: (MonadReader [String] m, MonadError ([String],String) m)
           => Identifier -> Specification
           -> m (JudgementForm,(Context,JudgementKind))
definition n (Specification s)
    = let c = JudgementForm n
      in case Map.lookup c s of
        Nothing -> throwWithTrace
            $ "No judgement-form '" ++ pprint n ++ "' in scope. Defined "
              ++ "judgement-forms are: " ++ commaList (map pprint $ Map.keys s)
              ++ "."
        Just x -> return (c,x)

{- Pretty printing -}

instance PrettyPrint Variable where
    pprint (Variable x) = pprint x

instance PrettyPrint JudgementForm where
    pprint (JudgementForm x) = pprint x

instance PrettyPrint JudgementKind where
    pprint Sort = "sort"
    pprint Proposition = "proposition"

instance PrettyPrint Judgement where
    pprint (Judgement h args _)
        = unwords $ pprint h : map (\(x,y) -> "{" ++ pprint x ++ " = "
                                              ++ pprint y ++ "}")
                                   (Map.toList args)

instance PrettyPrint Context where
    pprint (Context args _)
        = commaList $ map (\(x,y) -> pprint x ++ " : " ++ pprint y)
                          (Map.toList args)


instance PrettyPrint Specification where
    pprint (Specification dfs)
        = unlines
          $ map (\(h,(c,jk)) -> unwords [pprint c, "⊢", pprint h, pprint jk])
                (Map.toList dfs)
