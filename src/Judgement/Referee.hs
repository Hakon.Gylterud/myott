{-# LANGUAGE TupleSections #-}
{-# LANGUAGE FlexibleContexts #-}
module Judgement.Referee where

import Prelude hiding (init,head,mapM,sequence)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Map.Strict (Map)
import Control.Monad.Except
import Control.Monad.Reader

import Judgement
import Util.Trace
import Util.PrettyPrint

import Identifier


data MapError = NotInCodomain Identifier
              | NotInDomain Identifier
              | MissingFromDomainOfDefinition Identifier
              | PartialComposition String
              | IllTyped String
              | Unspecified String

box :: Monad m => (m a, m b) -> m (a, b)
box (a,b) = (,) <$> a <*> b

check :: Bool -> a -> Either a ()
check True _ = return ()
check False s = Left s

--compose :: Map a b -> Map b c -> Map a c
compose :: (Traversable t, MonadReader [String] m,
             MonadError ([String],String) m, Show b) => t Variable -> Map Identifier b -> m (t b)
compose f g = mapM (\b ->
    case Map.lookup (varToIdentifier b) g of
        Nothing -> throwWithTrace $
            "Could not find '" ++ show b ++ "' in the argument list " ++ show g
        (Just x) -> return x) f

compose' :: (Traversable t, MonadReader [String] m,
             MonadError ([String],String) m, Show b) => t Variable -> Map Identifier b -> m (t b)
compose' f g = mapM (\b ->
    case Map.lookup (varToIdentifier b) g of
        Nothing -> throwWithTrace $
            "Could not find '" ++ show b ++ "' in the argument list " ++ show g
        (Just x) -> return x) f

mapLeft :: (t -> a) -> Either t b -> Either a b
mapLeft f (Left a) = Left (f a)
mapLeft _ (Right b) = return b

-- | Extends a verified context with a
-- (variable, verified judgement)-pair.
--
-- The function checks:
--
--   * that the judgement is not a proposition which
--     is already in the context,
--   * that the variable is not already in the context.
pkAssume :: (MonadReader [String] m,
             MonadError ([String],String) m)
         => Identifier
         -> ((Specification, Context), Judgement)
         -> m (Specification, Context)
pkAssume ident ((spec , ctx), jgmt) = do
    when (kind jgmt == Proposition) $
        case Set.lookupMin (match ctx jgmt) of
            Nothing -> return ()
            (Just var) -> throwWithTrace $ "Cannot assume proposition '"
                                      ++ pprint ident ++ " "
                                      ++ pprint jgmt ++ "'.\n"
                                      ++ "  The proof object '"
                                      ++ pprint var ++ " "
                                      ++ pprint jgmt
                                      ++ " is alread in the context '"
                                      ++ pprint ctx ++ "'."
    (\ (c', _) -> (spec, c')) <$> extCtx ident jgmt ctx

checkMap :: Context -> Context -> Map Identifier Identifier
         -> Either MapError (Map Variable Variable)
checkMap domain codomain m =
    do let form' c v = mapLeft (const $ NotInCodomain v)
                               (runReader (runExceptT (form v c)) [])
       -- Check that the range is in codomain
       rawSubstitution <- mapM (form' codomain) m
       let domainVariables = Set.map varToIdentifier $ variables domain
       -- Check that the domain of arguments matches definition
       case Set.lookupMin
            $ Map.keysSet rawSubstitution `Set.difference` domainVariables of
                Nothing -> return ()
                (Just x) -> Left $ NotInDomain x
       case Set.lookupMin
            $ domainVariables `Set.difference` Map.keysSet rawSubstitution of
                Nothing -> return ()
                (Just x) -> Left $ MissingFromDomainOfDefinition x
       -- Verify the functoriality condition (C (f∘g) = Cf ∘ Cg)
       let h v (v2,Judgement h2 args2 k2) =
            do (v1,Judgement h1 args1 k1) <- form' domain v
               check (h1 == h2) $ IllTyped
                    $ "The sorts of the argument assignment '"
                      ++ pprint v1 ++ " = " ++ pprint v2 ++ "' do not match"
                      ++ "':\n   " ++ pprint  h1  ++ "\n     ≠\n   "
                      ++ pprint  h2
               argsSubstituted <- mapLeft
                    (IllTyped . snd)
                    (runExceptT (compose' args1 rawSubstitution) `runReader` [])
               check (args2 == Map.map fst argsSubstituted) $ IllTyped
                    $ "The sorts of the argument assignment '"
                      ++ pprint v1 ++ " = " ++ pprint v2 ++ "' do not match"
                      ++ ":\n   " ++ pprint (Judgement h1 args2 k1)
                      ++ "\n     ≠\n   "
                      ++ pprint (Judgement h2 (Map.map fst argsSubstituted) k2)
               return (v1,v2)
       verifiedSubstitution <- sequence $ Map.mapWithKey h rawSubstitution
       -- Construct the resulting substitution
       return $ Map.fromAscList $ map snd $ Map.toAscList verifiedSubstitution

pkApply :: (MonadReader [String] m,
            MonadError ([String],String) m)
        => Identifier
        -> Map Identifier Identifier
        -> (Specification, Context)
        -> m ((Specification, Context), Judgement)
pkApply h as (s,c) = do
    (cst,(ctx,k)) <- definition h s   -- Lookup of the definition
    vs <- case checkMap ctx c as of   -- Verify the substitution
        Left (NotInCodomain x) -> throwWithTrace
            $ "No variable '" ++ pprint x ++ "' in context '" ++ pprint c
              ++ "'."
        Left (NotInDomain x) -> throwWithTrace
            $ "JudgementForm '" ++ pprint h ++ "' takes no parameter '"
              ++ show x ++ "'."
        Left (MissingFromDomainOfDefinition x) -> throwWithTrace
            $ "JudgementForm '" ++ pprint h ++"' takes a parameter '"
              ++ pprint x ++ "', but none was given."
        Left (PartialComposition e) -> throwWithTrace e
        Left (IllTyped e) -> throwWithTrace
            $ "Typecheck failed on the arguments of '" ++ pprint h
              ++ "'.\n  " ++ e
        Left (Unspecified e) -> throwWithTrace e
        Right x -> return x
    return ((s,c), Judgement cst vs k)

protokernel
    :: (MonadReader [String] m,
        MonadError ([String],String) m)
    => Referee
               m
               Specification
               (Specification, Context)
               ((Specification, Context), Judgement)
protokernel
    = Referee {
            deps = (fst , fst),
            specification = id,
            context = snd,
            judgement = snd,
            init = Specification Map.empty,
            empty = (, emptyCtx),
            declare = \n b (s , c) -> extSpec n c b s,
            assume = pkAssume,
            -- Apply a judgement form to a number of arguments
            -- to construct a valid judgement form in a context
            apply = pkApply
    }
