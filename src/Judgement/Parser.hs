{-|
    Module      : Judgement.Parser
    Description : Parser for judgement specifications.
    Stability   : experimental

This module defines parsers for concepts related
to judgement forms.
-}
module Judgement.Parser (judgement
                        ,declaration
                        ,context
                        ,kind
                        ,sequent
                        ,specification) where

import Text.Megaparsec
import Text.Megaparsec.Char

import qualified Judgement
import Judgement.AST as AST
import Util.Parser
import Util.Monad ((<<))

judgement :: Parser Judgement
judgement = Judgement <$> identifier <*> arguments identifier identifier

declaration :: Parser Declaration
declaration
    = Declaration <$> identifier
                  <*> (space >> char ':' >> space >> judgement)

context :: Parser Context
context = Context <$> (declaration <* space) `sepBy'` (char ',' >> space)

kind :: Parser Judgement.JudgementKind
kind =  (string "sort" >> return Judgement.Sort)
    <|> (string "prop" >> return Judgement.Proposition)

sequent :: Parser Sequent
sequent
    = Sequent <$> context
              <*> (char '⊢' >> space >> identifier)
              <*> (space >> identifier `sepBy'` space1)
              <*> (space >> kind << space << char ';')

specification :: Parser Specification
specification
    = do space
         AST.Specification <$> manyTill (const <$> sequent <*> many newline) eof
