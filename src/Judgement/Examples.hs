module Judgement.Examples where

import Prelude hiding (init,print)
import qualified Data.Map.Strict as Map

import Identifier
import Util.Monad

import qualified Judgement.Monad as M
import Judgement.Monad hiding (declare, assume, apply)

declare = M.declare . Identifier
assume = M.assume . Identifier
apply n m
  = M.apply (Identifier n)
            (Map.map Identifier $ Map.mapKeysMonotonic Identifier m)


mlttSpec :: MakeSpec conv s c j s
mlttSpec =
    specification $
    declare "type" sort <^ context ^>
    declare "element" sort <^ ( context ^>
                                assume "A" <^ apply "type" Map.empty ) ^>
    declare "≡t" proposition <^ ( context ^>
                                  assume "A" <^ apply "type" Map.empty ^>
                                  assume "B" <^ apply "type" Map.empty ) ^>
    declare "≡e" proposition <^ ( context ^>
                                  assume "A" <^ apply "type" Map.empty ^>
                                  assume "B" <^ apply "type" Map.empty ^>
                                  assume "ε" <^ apply "≡t" (Map.fromList [("A","A"),("B","B")]) ^>
                                  assume "a" <^ apply "element" (Map.fromList [("A","A")]) ^>
                                  assume "b" <^ apply "element" (Map.fromList [("A","B")]) )

catSpec :: MakeSpec conv s c j s
catSpec =
   specification $
   declare "Ob" sort <^ context ^>
   declare "Hom" sort <^ (context ^>
                          assume "dom" <^ apply "Ob" Map.empty ^>
                          assume "codom" <^ apply "Ob" Map.empty)^>
   declare "≡" proposition <^ (context ^>
                               assume "dom"  <^ apply "Ob" Map.empty ^>
                               assume "codom" <^ apply "Ob" Map.empty ^>
                               assume "lhs"  <^ apply "Hom" (Map.fromList [("dom","dom"),("codom","codom")]) ^>
                               assume "rhs"  <^ apply "Hom" (Map.fromList [("dom","dom"),("codom","codom")]))

catFunSpec :: MakeSpec conv s c j s
catFunSpec =
   specification $
   declare "Cat" sort <^ context ^>
   declare "Fun" sort <^ (context ^>
                          assume "C" <^ apply "Cat" Map.empty ^>
                          assume "D" <^ apply "Cat" Map.empty)^>
   declare "Nat" sort <^ (context ^>
                          assume "C" <^ apply "Cat" Map.empty ^>
                          assume "D" <^ apply "Cat" Map.empty ^>
                          assume "f" <^ apply "Fun" (Map.fromList [("C","C"),("D","D")]) ^>
                          assume "g" <^ apply "Fun" (Map.fromList [("C","C"),("D","D")])) ^>
   declare "≡" proposition <^ (context ^>
                               assume "C" <^ apply "Cat" Map.empty ^>
                               assume "D" <^ apply "Cat" Map.empty ^>
                               assume "f" <^ apply "Fun" (Map.fromList [("C","C"),("D","D")]) ^>
                               assume "g" <^ apply "Fun" (Map.fromList [("C","C"),("D","D")]) ^>
                               assume "α" <^ apply "Nat" (Map.fromList [("C","C"),("D","D"),("f","f"),("g","g")]) ^>
                               assume "β" <^ apply "Nat" (Map.fromList [("C","C"),("D","D"),("f","f"),("g","g")]))


failTest1 =
   context ^>
   assume "A" <^ apply "type" Map.empty ^>
   assume "A" <^ apply "type" Map.empty

failTest2 =
   context ^>
   assume "A" <^ apply "type" Map.empty ^>
   assume "a" <^ apply "element" Map.empty

failTest3 =
   context ^>
   assume "A" <^ apply "type" Map.empty ^>
   assume "a" <^ apply "element" (Map.fromList [("A","A"),("B","A")])

failTest4 =
   context ^>
   assume "A" <^ apply "type" Map.empty ^>
   assume "a" <^ apply "elemnt" (Map.fromList [("A","A")])

failTest5 =
   context ^>
   assume "A" <^ apply "type" Map.empty ^>
   assume "B" <^ apply "type" Map.empty ^>
   assume "a" <^ apply "element" (Map.fromList [("A","A")]) ^>
   assume "b" <^ apply "element" (Map.fromList [("A","B")]) ^>
   assume "ε" <^ apply "≡t" (Map.fromList [("A","A"),("B","B")]) ^>
   assume "δ" <^ apply "≡e" (Map.fromList [("A","A"),("B","A"),("a","a"),("b","b"),("ε","ε")])

failTest6 =
   context ^>
   assume "A" <^ apply "type" Map.empty ^>
   assume "B" <^ apply "type" Map.empty ^>
   assume "a" <^ apply "element" (Map.fromList [("A","A")]) ^>
   assume "b" <^ apply "element" (Map.fromList [("A","B")]) ^>
   assume "ε" <^ apply "≡t" (Map.fromList [("A","A"),("B","B")]) ^>
   assume "δ" <^ apply "≡e" (Map.fromList [("A","A"),("B","B"),("a","a"),("b","B"),("ε","ε")])
