{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Argument
    Description : Data types for representing arguments for operations
                  and judgements.
    Stability   : experimental

This module defines data structures related to
argument handling in Myott.
-}

module Argument (NamedArguments
                ,DependencyRelation
                ,Arguments(ArgList,ArgMap)
                ) where


import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

{- | 'NamedArguments' are arguments with specific names.
     for instance @f{x ↦ 3, y ↦ 2, name ↦ "foo"}@. -}
type NamedArguments name value
    = Map name value

{- | A 'DependencyRelation' is a graph where the edges
     represent some kind of dependency relation between
     the objects. In Myott these dependencies come
     from the dependencies between judgement forms. -}
type DependencyRelation object index
    = Map object (Map index object)

{-|
    `Arguments` is either a list of arguments,or
    a mapping of argument identifiers to arguments.
-}
data Arguments argname a
    -- | An argument list of the form f(3,2,"foo")
    = ArgList [a]
    -- | An argument list of the form f{x ↦ 3, y ↦ 2, name ↦ "foo"}
    | ArgMap (NamedArguments argname a)
      deriving (Eq,Show)

instance Functor (Arguments argname) where
    fmap f (ArgList l) = ArgList $ map f l
    fmap f (ArgMap m) = ArgMap $ Map.map f m

instance Foldable (Arguments argname) where
    foldr f b (ArgList ar) = foldr f b ar
    foldr f b (ArgMap ar) = Map.foldr f b ar
