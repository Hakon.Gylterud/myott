{-# LANGUAGE FlexibleContexts,TupleSections #-}
{-|
    Module      : Argument.Inference
    Description : Data types and functions for simple argument inference
                  based on following dependencies.
    Stability   : experimental

This module defines a datatype 'Resolution' which stores dependency information
that can be used to infer arguments.
-}


module Argument.Inference
    (Resolution
    ,infer
    ,inferAll
    ,mergeArguments
    ,createResolution
    ) where

import Argument
import Util.PrettyPrint
import Util.Trace

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

import qualified Data.Set as Set
import Data.Set(Set)

import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.State
import Control.Arrow ((&&&))
import Control.Applicative

{- | 'Resolution' is an intermediate data-structure encoding
     how to infer arguments of judgements and operations.
     The @i@ parameter type represents dependency indices
     which can be followed in a given context to infer an
     argument.

     The @f@ parameter is meant to be 'Maybe' when representing
     inference without a specific context, and @Either object@ when
     representing inference in a specific context with elements
     represented by @object@. The 'mergeArguments' function can use
     a partial map of arguments to make the inference resolution
     concrete to a specific context.

     When @f = Maybe@ arguments mapped to @Nothing@ represent
     mandatory (uninferred) arguments.

     When @f = Either object@ the arguments mapped to @Left x@
     represent given mandatory arguments, as well as already
     inferred arguments.

     In both cases, an argument mapped to @pure (n,i)@ denotes
     that the argument is to be inferred using the dependency @i@
     on the argument @n@.
-}
type Resolution f
                argname
                index
    = Map argname (f (argname,index))

createResolution' :: (Ord object)
                  => DependencyRelation object index
                  -> object
                  -> [[(object, (object , index))]]
createResolution' cxt i
    = do args <- maybe empty pure (Map.lookup i cxt)
         let vs = map (snd &&& ((i,) . fst)) (Map.toList args)
         q <- forM vs (createResolution' cxt . fst)
         return (vs ++ concat q)

{- | Create a resolution from a context -}
createResolution :: (Ord object
                    ,Ord index)
                 => DependencyRelation object index
                 -> Set object
                 -> Resolution Maybe object index
createResolution cxt vs
    = Map.union
        (Map.fromList $ map (, Nothing) (Set.toList vs))
        (Map.map Just
            $ Map.fromList
            $ concat
            $ concat
            $ forM (Set.toList vs)
                   (createResolution' cxt))

{- | Infer an argument based on a 'DependencyRelation' and
     a 'Resolution'. The 'infer' function also returns a modified
     'Resolution' which memoises the result of the inference.
     In this way, each inference is performed only once. -}
infer :: (MonadError ([String] , String) m
         ,MonadReader [String] m
         ,Ord object,   PrettyPrint object
         ,Ord index,    PrettyPrint index
         ,Ord argname,  PrettyPrint argname)
      => DependencyRelation object index
      -- ^ The dependencies to follow when inferring.
      -> argname
      -- ^ Name of the argument to infer.
      -> Resolution (Either object) argname index
      -- ^ The resolution of inferences so far.
      -> m (object, Resolution (Either object) argname index)
infer dependencies argument resolution
    = tag ("When inferring the argument '" ++ pprint argument ++ "':")
    $ do
        -- Look up the inference strategy for this argument
        x <- force  (Map.lookup argument resolution)
                    ("The argument '"
                        ++ pprint argument
                        ++ "' has not been tagged for inference.")
        case x of
            -- If the argument is already inferred, return it.
            Left inferedObject -> return (inferedObject , resolution)
            {-
            Otherwise, we are to follow the dependencies of
            some other argument, here @argument'@. -}
            Right (argument' , index) -> do
                (infered' , resolution') <-
                    infer dependencies argument' resolution
                dependencies' <-
                    force (Map.lookup infered' dependencies)
                          ("Infering failed: The infered value of '"
                           ++ pprint argument' ++ "' namely '"
                           ++ pprint infered'
                           ++ "', was not found in the context.'")
                infered <-
                    force (Map.lookup index dependencies')
                          ("Infering failed: The object '" ++ pprint infered'
                           ++ "' has no argument '" ++ pprint index
                           ++ "'. Its arguments are:\n"
                           ++ unlines (map (\(r,s) -> pprint r ++ " ↦ "
                                                      ++ pprint s)
                                      (Map.toList dependencies')) )
                return (infered
                       ,Map.insert argument
                                   (Left infered)
                                   resolution')

flipEither = either Right Left

{- | Infer all arguments in a resolution, giving a complete
     collection of arguments. -}
inferAll :: (MonadError ([String] , String) m
            ,MonadReader [String] m
            ,Ord object,   PrettyPrint object
            ,Ord index,    PrettyPrint index
            ,Ord argname,  PrettyPrint argname)
          => DependencyRelation object index
          -> Resolution (Either object) argname index
          -> m (NamedArguments argname object)
inferAll dependencies resolution
    = do ref <- execStateT (traverse (StateT . infer dependencies)
                                     (Map.keys resolution))
                           resolution
         forM ref (flip force "Some arguments were not inferred."
                  . flipEither)


{- | Merge arguments from a set of named arguments into a general
     'Resolution' giving a specific 'Resolution' ready to infer
     arguments. -}
mergeArguments :: (MonadError ([String] , String) m
                  ,MonadReader [String] m
                  ,Ord argname, PrettyPrint argname)
               => NamedArguments argname object
               -> Resolution Maybe argname index
               -> m (Resolution (Either object) argname index)
mergeArguments arguments resolution
    = Map.traverseWithKey
        (\k -> flip force ("The argument '" ++ pprint k
                           ++ "' was not successfully inferred."))
    $ Map.union (Map.map (Just . Left) arguments)
                (Map.map (fmap Right) resolution)
