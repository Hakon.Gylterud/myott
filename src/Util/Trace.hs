{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Util.Trace
    Description : Functions for Reader-based stack tracing.
    Stability   : experimental

This module defines 'tag' and 'throwWithTrace' which
respectively allow tagging (possibly nested) blocks
of code, and producing a trace of these tags on a
stack when errors are encountered.

These functions work by keeping a @[s]@ variable
in a @MonadReader@, and @ask@ing for this stack
when throwing.


__Example:__


@
    f :: (MonadReader [String] m, MonadError ([String], String) m) =>
        Integer -> m Integer
    f x = tag "In function f"
        $ do
            when (x == 1) (throwWithTrace "x should not be 1!")
            return (x + 1)

    g :: (MonadReader [String] m, MonadError ([String], String) m) =>
        Integer -> m Integer
    g x = tag "In function g"
        $ do
            y <- f x
            when (y == 3) (throwWithTrace "y should not be 3!")
            return (y*2)
@

Running these gives:

@
> runReader (runExceptT (f 3)) []
  Right 4

 > runReader (runExceptT (f 1)) []
Left (["In function f"],"x should not be 1!")

 > runReader (runExceptT (g 1)) []
Left (["In function f","In function g"],"x should not be 1!")

 > runReader (runExceptT (g 2)) []
Left (["In function g"],"y should not be 3!")
@
-}
module Util.Trace
    (throwWithTrace
    ,tag
    ,force
    ,TraceError
    ,TraceErrorT) where

import Control.Monad.Reader
import Control.Monad.Except

{-|
    Throw an error and include a trace.
-}
throwWithTrace :: (MonadError (s , e) m, MonadReader s m)
               => e -- ^ The error to throw.
               -> m a
throwWithTrace e = do s <- ask
                      throwError (s , e)

{-|
    Tag a block with a value added to
    the stack trace
-}
tag :: (MonadReader [s] m)
    => s    -- ^ The value to push to the trace stack.
    -> m a  -- ^ A code block
    -> m a  -- ^ Tagged block
tag s = local (s:)

{-|
    Force a value from a Foldable structure (such as 'Maybe'),
    possibly throwing an error if no value is available.

    If more than one element of the structure is encountered,
    'force' returns the last element found.
    For instance @force [1,2,3] e = return 3@.
-}
force :: (MonadError (s , e) m
         ,MonadReader s m
         ,Foldable t)
      => t a -- ^ A Foldable structure to extract a value from.
      -> e   -- ^ Error to throw if no element is available.
      -> m a
force = flip (foldl (const return) . throwWithTrace)

{-|
    'TraceError' is a straightforward monad stack implementing
    tracing errors.
-}
type TraceError s e = ExceptT ([s],e) (Reader [s])

type TraceErrorT s e m = ExceptT ([s],e) (ReaderT [s] m)