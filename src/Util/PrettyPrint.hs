module Util.PrettyPrint where

class PrettyPrint a where
    pprint :: a -> String

commaList :: [String] -> String
commaList [] = ""
commaList [x] = x
commaList (x:xs) = x ++ ", " ++ commaList xs

assignmentList :: (PrettyPrint a, PrettyPrint b) => [(a,b)] -> String
assignmentList
    = unwords . map (\(x,y) ->
        "(" ++ pprint x ++ " = " ++ pprint y ++ ")")
