{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Util.AST
    Description : Utility functions for abstract syntax trees.
    Stability   : experimental

This module is empty for the time being.
-}
module Util.AST where