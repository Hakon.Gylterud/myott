{-|
    Module      : Util.Parser
    Description : Utility functions for parsing.
    Stability   : experimental

This module contains specialised version of @many@
and @some@, called `many'` and `some'`. It also contains
a parser for argument lists, which is used by judgements
and operators in Myott.

We need versions of @many : Parser a -> Parser [a]@
and @sepBy : Parser s -> Parser a -> Parser [a]@ which
behave slightly different from the standard ones.
The idea is that @many' p@ will consume eagerly, but
stop gracefully when encountering something which
does not fit the element parser.
-}
module Util.Parser where

import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char

import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.List.NonEmpty (NonEmpty((:|)))

import Identifier
import Argument


type Parser = Parsec Void String

{- | Try one parsing then proceed, if the first parser
     fails, proceed instead with a fallback parser. -}
tryDefault :: Parser a  -- ^ Attempt this parser first.
           -> Parser b  -- ^ Fallback parser.
           -> (a -> Parser b) -- ^ Next step if successful.
           -> Parser b -- ^ Combined parser.
tryDefault a b f = optional (try a) >>= maybe b f

{-| Like `many` but consume until element
    parsing fails, and then return.
-}
many' :: Parser a -> Parser [a]
many' p = tryDefault p (return []) ((<$> many' p) . (:))

{-| Like `sepBy` but consume until element
    parsing fails, and then return.
-}
sepBy' :: Parser a -> Parser sep -> Parser [a]
sepBy' p sep
    = tryDefault p
                (return [])
                ((<$> (many' (sep >> p) <|> return [])) . (:))

{- | Like `sepBy1` but consume until element
     parsing fails, and then return. Only fails if
     it cannot parse any instance of input parser. -}
sepBy1' :: Parser a -> Parser sep -> Parser [a]
sepBy1' parsr sepParsr = do
    val <- parsr
    vals <- many' (sepParsr >> parsr)
    return (val : vals)

{- | Parse one pair of curly brackets around input parser.
-}
betweenCurlies :: Parser a -> Parser a
betweenCurlies = between (char '{' >> space) (space >> char '}')

{- | Parse zero or one pair of parentheses around input
     parser.
-}
betweenOptionalParens :: Parser a -> Parser a
betweenOptionalParens parsr = do
    maybeParens <- optional $ char '(' <* space
    case maybeParens of
        Nothing -> parsr
        Just _ -> parsr <* space <* char ')'

{- | Parse one pair of parentheses around input parser. -}
betweenParens :: Parser a -> Parser a
betweenParens = between (char '(' >> space) (space >> char ')')


{- | Parse arguments for a judgement or operator. These are
     either lists like "1 2 3" or maps "{x = 1, y = 2, z = 4}".
-}
arguments :: Parser a -> Parser a -> Parser (Arguments Identifier a)
arguments argMapParsr argListParsr
    = try (ArgMap . Map.fromList
                <$> (space >> betweenCurlies ((do
                        x <- identifier
                        space >> char '=' >> space
                        y <- betweenParens argMapParsr <|> argMapParsr
                        space
                        return (x,y)) `sepBy'` (char ',' >> space))))
          <|> (ArgList <$> tryDefault space1
                                      (return [])
                                      (const (argListParsr `sepBy'` space1)))

-- | The list of Myott reserved characters.
reserved = "(){}≔=⊢ \n\t,:;─"

reservedWords = Set.fromList ["sort","prop"]

-- | Parse an identifier.
identifier :: Parser Identifier
identifier = label "identifier"
           $ Identifier
           <$> do x <- some (noneOf reserved)
                  if Set.member x reservedWords
                  then unexpected (Label (head x :| tail x))
                  else return x