{- |
    Module      : Theory.AST
    Description : Abstract syntax trees for theories.
    Stability   : experimental

This module defines abstract syntax trees for
theory specifications and functions to run these
on a 'Theory.Referee'.

The abstract syntax is produced by the parser, but
is not the final product. The abstract syntax is
fed to a referee, which produces a complete
combinatorial representation of the theory.
-}

module Theory.AST where

import Identifier
import Sequent.AST (Judgement, Term, Environment)


{- | An abstract, syntactic theory consists of
     a list of rules. -}
newtype Theory f = Theory [Rule f]

{- | An abstract, syntactic rule consists of a
     frame (that which is 'above the line') and a
     sequent, but where the conclusion cannot be a
     term (that which is 'below the line'). -}
data Rule f = Rule (Frame f) (Environment f) Identifier [Identifier] (Judgement f)

{- | An abstract, syntactic frame consists
     of a list of sequents. -}
newtype Frame f = Frame [Sequent f]

{- | An abstract, syntactic sequent consists of an
     environment (that which is to the left of \'⊢\'), and
     an identifier (with possible arguments) and either a
     judgement or a term (that which is to the right of
     \'⊢\'). -}
data Sequent f = Sequent (Environment f) Identifier [Identifier] (Either (Judgement f) (Term f))
