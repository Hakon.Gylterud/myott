#!/bin/sh

cabal new-test \
&& cabal new-run myott check examples/cat.j examples/cat.s \
&& cabal new-run myott check examples/cat.j examples/terminal.s

rm -f myott.tix || exit 0
