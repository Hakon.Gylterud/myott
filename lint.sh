#!/bin/sh

linted="src/Util/*.hs
       src/Argument/*.hs \
       src/Judgement/*.hs \
       src/Sequent/Monad.hs src/Sequent/Referee.hs\
       src/Sequent/AST.hs src/Sequent/Parser.hs \
       src/Theory/Monad.hs src/Theory/AST.hs src/Theory/Parser.hs \
       src/*.hs"

hlint $linted
