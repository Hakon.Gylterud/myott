module Sequent.TestArgumentScoping (argumentScopingTests) where

import Argument
import Data.Functor.Identity (Identity(..))
import Identifier
import qualified Data.Map.Strict as Map

import Sequent.ArgumentScoping
import Sequent.AST
import Sequent.ASTInstances
import Test.Tasty
import Test.Tasty.HUnit

argumentScopingTests :: TestTree
argumentScopingTests = testGroup "Argument scoping" 
                                 [testSpec1,
                                  testSpec2]

{- Sequent corresponding to

"x : Ob ⊢ id x : Hom {dom = x, codom = x}"

before argument scoping. -}
seq1 :: Sequent Identity
seq1 = Sequent (Environment [(Identifier "x", Left (Judgement (Identifier "Ob") (ArgList [])))])
               (Identifier "id")
               [Identifier "x"]
               (Judgement (Identifier "Hom")
                          (ArgMap (Map.fromList [(Identifier "dom", Identity (Term (Identifier "x")
                                                                                   (ArgList []))),
                                                 (Identifier "codom", Identity (Term (Identifier "x")
                                                                                     (ArgList [])))])))

{- Sequent corresponding to

"x : Ob ⊢ id x : Hom {dom = x, codom = x}"

before argument scoping. -}
scopedSeq1 :: Sequent (Either Identifier)
scopedSeq1 = Sequent (Environment [(Identifier "x", Left (Judgement (Identifier "Ob") (ArgList [])))])
                     (Identifier "id")
                     [Identifier "x"]
                     (Judgement (Identifier "Hom")
                                (ArgMap (Map.fromList [(Identifier "dom", Left (Identifier "x")),
                                                       (Identifier "codom", Left (Identifier "x"))])))

testSpec1 :: TestTree
testSpec1 = testCase "Scoping the arguments in specification \
                     \containing only sequent \
                     \\"x : Ob ⊢ id x : Hom {dom = x, codom = x}\""
                     (inSpecification (Specification [seq1])
                     @?= Specification [scopedSeq1])

{- Environment corresponding to

"x : Ob, y : Ob, f : Hom x y, i ≔ id x"

before argument scoping. -}
env2 :: Environment Identity
env2 = Environment [(Identifier "x", Left (Judgement (Identifier "Ob") (ArgList []))),
                    (Identifier "y", Left (Judgement (Identifier "Ob") (ArgList []))),
                    (Identifier "f", Left (Judgement (Identifier "Hom")
                                                     (ArgList [Identity (Term (Identifier "x")
                                                                              (ArgList [])),
                                                               Identity (Term (Identifier "y")
                                                                              (ArgList []))]))),
                    (Identifier "i", Right (Term (Identifier "id")
                                                 (ArgList [Identity (Term (Identifier "x")
                                                                          (ArgList []))])))]

{- Environment corresponding to

"x : Ob, y : Ob, f : Hom x y, i ≔ id x"

after argument scoping. -}
scopedEnv2 :: Environment (Either Identifier)
scopedEnv2 = Environment [(Identifier "x", Left (Judgement (Identifier "Ob") (ArgList []))),
                          (Identifier "y", Left (Judgement (Identifier "Ob") (ArgList []))),
                          (Identifier "f", Left (Judgement (Identifier "Hom")
                                                           (ArgList [Left (Identifier "x"),
                                                                     Left (Identifier "y")]))),
                          (Identifier "i", Right (Term (Identifier "id")
                                                       (ArgList [Left (Identifier "x")])))]

{- Sequent corresponding to

x : Ob, y : Ob, f : Hom x y, i ≔ id x ⊢ right-id : ≡ (comp f i) f

before argument scoping. -}
seq2 :: Sequent Identity
seq2 = Sequent env2 
               (Identifier "right-id")
               []
               (Judgement (Identifier "≡")
                          (ArgList [Identity (Term (Identifier "comp")
                                                   (ArgList [Identity (Term (Identifier "f")
                                                                            (ArgList [])),
                                                             Identity (Term (Identifier "i")
                                                                            (ArgList []))])),
                                    Identity (Term (Identifier "f")
                                                   (ArgList []))]))

{- Sequent corresponding to

x : Ob, y : Ob, f : Hom x y, i ≔ id x ⊢ right-id : ≡ (comp f i) f

after argument scoping. -}
scopedSeq2 :: Sequent (Either Identifier)
scopedSeq2 = Sequent scopedEnv2
                     (Identifier "right-id")
                     []
                     (Judgement (Identifier "≡")
                                (ArgList [Right (Term (Identifier "comp")
                                                      (ArgList [Left (Identifier "f"),
                                                                Left (Identifier "i")])),
                                          Left (Identifier "f")]))

testSpec2 :: TestTree
testSpec2 = testCase "Scoping the arguments in specification \
                     \containing only sequent \
                     \\"x : Ob, y : Ob, i ≔ id x, c ≔ comp f i \
                     \⊢ right-id : ≡ c f\""
                     (inSpecification (Specification [seq2])
                     @?= Specification [scopedSeq2])
