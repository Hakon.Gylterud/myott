module Sequent.TestParser (parserTests) where

import Data.Functor.Identity ( Identity(Identity) )
import qualified Data.Map.Strict as Map
import Test.Tasty ( TestTree, testGroup )
import Test.Tasty.HUnit ( testCase, (@?=) )
import Text.Megaparsec ( parse )

import Argument
import Identifier
import Sequent.AST
import Sequent.ASTInstances
import Sequent.Parser

parserTests :: TestTree
parserTests = testGroup "Parser" [testJgmt1,
                                  testSeq1,
                                  testSeq2]

testJgmt1 :: TestTree
testJgmt1 = testCase "Result of parsing \
                     \\"Hom {dom = φ (comp {fst = id x, snd = f}), codom = ψ f}\""
                     (parse judgement "" "Hom {dom = φ (comp {fst = id x, snd = f}), codom = ψ f}"
                     @?= Right (Judgement (Identifier "Hom")
                                          (ArgMap (Map.fromList [(Identifier "dom", Identity (Term (Identifier "φ")
                                                                                                   (ArgList [Identity (Term (Identifier "comp")
                                                                                                                            (ArgMap (Map.fromList [(Identifier "fst", Identity (Term (Identifier "id")
                                                                                                                                                                                     (ArgList [Identity (Term (Identifier "x")
                                                                                                                                                                                                              (ArgList []))]))),
                                                                                                                                                   (Identifier "snd", Identity (Term (Identifier "f")
                                                                                                                                                                                     (ArgList [])))])))]))),
                                                                 (Identifier "codom", Identity (Term (Identifier "ψ")
                                                                                                     (ArgList [Identity (Term (Identifier "f")
                                                                                                                              (ArgList []))])))]))))

testSeq1 :: TestTree
testSeq1 = testCase "Result of parsing \
                    \\"x : Ob ⊢ id x : Hom {dom = x, codom = x} ;\""
                    (parse sequent "" "x : Ob ⊢ id x : Hom {dom = x, codom = x} ;"
                    @?= Right (Sequent (Environment [(Identifier "x", Left (Judgement (Identifier "Ob") (ArgList [])))])
                                       (Identifier "id")
                                       [Identifier "x"]
                                       (Judgement (Identifier "Hom")
                                                  (ArgMap (Map.fromList [(Identifier "dom", Identity (Term (Identifier "x")
                                                                                                           (ArgList []))),
                                                                         (Identifier "codom", Identity (Term (Identifier "x")
                                                                                                             (ArgList [])))])))))

env2 :: Environment Identity
env2 = Environment [(Identifier "x", Left (Judgement (Identifier "Ob") (ArgList []))),
                    (Identifier "y", Left (Judgement (Identifier "Ob") (ArgList []))),
                    (Identifier "f", Left (Judgement (Identifier "Hom")
                                                     (ArgList [Identity (Term (Identifier "x")
                                                                              (ArgList [])),
                                                               Identity (Term (Identifier "y")
                                                                              (ArgList []))]))),
                    (Identifier "i", Right (Term (Identifier "id")
                                                 (ArgList [Identity (Term (Identifier "x")
                                                                          (ArgList []))])))]

testSeq2 :: TestTree
testSeq2 = testCase "Result of parsing \
                    \\"x : Ob, y : Ob, f : Hom x y, i ≔ id x \
                    \⊢ right-id f : ≡ (comp f i) i ;\""
                    (parse sequent "" "x : Ob, y : Ob, f : Hom x y, i ≔ id x ⊢ right-id f : ≡ (comp f i) i ;"
                    @?= Right (Sequent env2
                                       (Identifier "right-id")
                                       [Identifier "f"]
                                       (Judgement (Identifier "≡")
                                                  (ArgList [Identity (Term (Identifier "comp")
                                                                           (ArgList [Identity (Term (Identifier "f")
                                                                                                    (ArgList [])),
                                                                                     Identity (Term (Identifier "i")
                                                                                                    (ArgList []))])),
                                                            Identity (Term (Identifier "i")
                                                                           (ArgList []))]))))
