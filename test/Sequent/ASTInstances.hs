{-# LANGUAGE FlexibleInstances #-}

module Sequent.ASTInstances where

import Data.Functor.Identity

import Identifier
import Sequent.AST

-- Instances for 'Identity'.

instance Eq (Term Identity) where
    (Term hd args) == (Term hd' args')
        = hd == hd' && args == args'

instance Show (Term Identity) where
    show (Term hd args) = "Term { termHead = "
                          ++ show hd
                          ++ ", termArgs = "
                          ++ show args
                          ++ " }"

instance Eq (Judgement Identity) where
    (Judgement hd args) == (Judgement hd' args')
        = hd == hd' && args == args'

instance Show (Judgement Identity) where
    show (Judgement hd args) = "Judgement ("
                               ++ show hd
                               ++ ") ("
                               ++ show args
                               ++ ")"

instance Eq (Environment Identity) where
    (Environment envList) == (Environment envList')
        = envList == envList'

instance Show (Environment Identity) where
    show (Environment envList) = "Environment " ++ show envList

instance Eq (Sequent Identity) where
    (Sequent env hd args jgmt) == (Sequent env' hd' args' jgmt')
        = env == env' && hd == hd' && args == args' && jgmt == jgmt'

instance Show (Sequent Identity) where
    show (Sequent env hd args jgmt) = "Sequent ("
                                      ++ show env
                                      ++ ") ("
                                      ++ show hd
                                      ++ ") ("
                                      ++ show args
                                      ++ ") ("
                                      ++ show jgmt
                                      ++ ")"

-- Instances for 'Either Identifier'.

instance Eq (Term (Either Identifier)) where
    (Term hd args) == (Term hd' args')
        = hd == hd' && args == args'

instance Show (Term (Either Identifier)) where
    show (Term hd args) = "Term { termHead = "
                          ++ show hd
                          ++ ", termArgs = "
                          ++ show args
                          ++ " }"

instance Eq (Judgement (Either Identifier)) where
    (Judgement hd args) == (Judgement hd' args')
        = hd == hd' && args == args'

instance Show (Judgement (Either Identifier)) where
    show (Judgement hd args) = "Judgement ("
                               ++ show hd
                               ++ ") ("
                               ++ show args
                               ++ ")"

instance Eq (Environment (Either Identifier)) where
    (Environment envList) == (Environment envList')
        = envList == envList'

instance Show (Environment (Either Identifier)) where
    show (Environment envList) = "Environment " ++ show envList

instance Eq (Sequent (Either Identifier)) where
    (Sequent env hd args jgmt) == (Sequent env' hd' args' jgmt')
        = env == env' && hd == hd' && args == args' && jgmt == jgmt'

instance Show (Sequent (Either Identifier)) where
    show (Sequent env hd args jgmt) = "Sequent ("
                                      ++ show env
                                      ++ ") ("
                                      ++ show hd
                                      ++ ") ("
                                      ++ show args
                                      ++ ") ("
                                      ++ show jgmt
                                      ++ ")"

instance Eq (Specification (Either Identifier)) where
    (Specification seqs) == (Specification seqs')
        = seqs == seqs'

instance Show (Specification (Either Identifier)) where
    show (Specification seqs) = "Specification " ++ show seqs
