module TestTheory where

import Test.Tasty
import Theory.TestParser

theoryTests :: TestTree
theoryTests = testGroup "Theory" [parserTests]