module Judgement.ASTInstances where

import Judgement.AST

instance Eq Judgement where
    (Judgement hd args) == (Judgement hd' args') = hd == hd' && args == args'

instance Show Judgement where
    show (Judgement hd args) = "Judgement " ++ show hd ++ " (" ++ show args ++ ")"
