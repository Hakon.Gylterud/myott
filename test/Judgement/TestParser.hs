module Judgement.TestParser (parserTests) where

import Test.Tasty ( TestTree, testGroup )
import Test.Tasty.HUnit ( testCase, (@?=) )
import Text.Megaparsec ( parse )

import Argument
import Identifier
import Judgement.AST
import Judgement.ASTInstances
import Judgement.Parser

parserTests :: TestTree
parserTests = testGroup "Parser" [testJgmt1, testJgmt2]

testJgmt1 :: TestTree
testJgmt1 = testCase "Result of parsing \"Ob\""
                 (parse judgement "" "Ob"
                 @?= Right (Judgement (Identifier "Ob")
                                      (ArgList [])))

testJgmt2 :: TestTree
testJgmt2 = testCase "Result of parsing \"Hom x y\""
                 (parse judgement "" "Hom x y"
                 @?= Right (Judgement (Identifier "Hom")
                                      (ArgList [Identifier "x", Identifier "y"])))
