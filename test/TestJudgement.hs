module TestJudgement (judgementTests) where

import Test.Tasty

import Judgement.TestParser (parserTests)

judgementTests :: TestTree
judgementTests = testGroup "Judgement" [parserTests]
