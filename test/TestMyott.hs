module Main where

import Test.Tasty

import TestJudgement (judgementTests)
import TestSequent (sequentTests)
import TestTheory (theoryTests)

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [judgementTests, sequentTests, theoryTests]
