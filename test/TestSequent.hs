module TestSequent (sequentTests) where

import Test.Tasty

import Sequent.TestParser (parserTests)
import Sequent.TestArgumentScoping (argumentScopingTests)

sequentTests :: TestTree
sequentTests = testGroup "Sequent" [parserTests, argumentScopingTests]
