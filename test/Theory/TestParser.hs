module Theory.TestParser where

import Data.Functor.Identity ( Identity(Identity) )
import Test.Tasty ( TestTree, testGroup )
import Test.Tasty.HUnit ( testCase, (@?=), assertFailure, assertBool )
import Text.Megaparsec ( parse, errorBundlePretty )

import Argument
import Identifier
import qualified Theory.AST as T
import Theory.Parser
import Sequent.AST
import Theory.ASTInstances

parserTests :: TestTree
parserTests = testGroup "Parser" [testSeq1, testSeq2, testRule1, testRule2]

testSeq1 :: TestTree
testSeq1 = 
    testCase "Result of parsing \"⊢ p ≔ Π A B\""
    (parse sequent "" "⊢ p ≔ Π A B" 
    @?= Right (T.Sequent 
        (Environment [])
        (Identifier "p")
        []
        (Right (Term (Identifier "Π") (
            ArgList [
                Identity (Term (Identifier "A") (ArgList [])), 
                Identity (Term (Identifier "B") (ArgList []))
                ]
            )))  
    ))

seq2 :: T.Sequent Identity
seq2 = T.Sequent 
    (Environment [
        (
            Identifier "x", 
            Left (
                Judgement 
                (Identifier "Element") 
                (ArgList [Identity (Term (Identifier "A") (ArgList []))])
            )
        )
    ])
    (Identifier "B")
    []
    (Left (Judgement (Identifier "Type") (ArgList [])))

testSeq2 :: TestTree
testSeq2 = testCase 
    "Result of parsing \"x : Element A ⊢ B : Type\""
    (parse sequent "" "x : Element A ⊢ B : Type" @?= Right seq2)

testRule1 :: TestTree
testRule1 = 
    testCase "Result of parsing \n\
    \\"{\n\
        \⊢ A : Type ;\n\
        \x : Element A ⊢ B : Type\n\
        \────────────────────────\n\
        \⊢ Π : Type\n\
    \}\"" 
    (parse rule "" 
    "{\n\
        \⊢ A : Type ;\n\
        \x : Element A ⊢ B : Type\n\
        \────────────────────────\n\
        \⊢ Π : Type\n\
    \}" 
    @?= Right (
        T.Rule 
        (T.Frame [
            (T.Sequent (Environment []) (Identifier "A") [] (Left (Judgement (Identifier "Type") (ArgList [])))),
            seq2
        ])
        (Environment [])
        (Identifier "Π")
        []
        (Judgement (Identifier "Type") (ArgList []))))

testRule2 :: TestTree
testRule2 = 
    testCase "Parsing \n\
    \\"{\n\
        \⊢ A : Type ;\n\
        \x : Element A ⊢ B : Type\n\
        \────────────────────────\n\
        \⊢ Π ≔ Type\n\
    \}\" should not work as there is a term and not a judgement in the conclusion."
    (case (parse rule "" 
    "{\n\
        \⊢ A : Type ;\n\
        \x : Element A ⊢ B : Type\n\
        \────────────────────────\n\
        \⊢ Π ≔ Type\n\
    \}") of
        Left _ -> return ()
        Right _ -> assertFailure "Expected failure when parsing a rule with a term in the conclusion.")