{-# LANGUAGE FlexibleInstances #-}

module Theory.ASTInstances where

import Theory.AST
import Sequent.ASTInstances
import Data.Functor.Identity ( Identity(Identity) )

instance Show (Sequent Identity) where
     show (Sequent e i a v) = 
        "Sequent (" 
        ++ show e ++ ") ("
        ++ show i ++ ") ("
        ++ show a ++ ") ("
        ++ show v ++ ")"

instance Eq (Sequent Identity) where
    Sequent e i a v == Sequent e' i' a' v' = 
        e == e' && i == i' && a == a' && v == v'

instance Show (Frame Identity) where
    show (Frame sqs) = "Frame [" ++ show sqs ++ "]"

instance Eq (Frame Identity) where
    Frame sqs == Frame sqs' =
        sqs == sqs'

instance Show (Rule Identity) where
    show (Rule f e i a j) =
        "Rule ("
        ++ show f ++ ") ("
        ++ show e ++ ") ("
        ++ show i ++ ") ("
        ++ show a ++ ") ("
        ++ show j ++ ") "

instance Eq (Rule Identity) where
    Rule f e i a j == Rule f' e' i' a' j' =
        f == f' && e == e' && i == i' && a == a' && j == j'